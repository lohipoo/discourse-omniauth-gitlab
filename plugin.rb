# name:  discourse-omniauth-gitlab
# about: Authenticate Discourse with GitLab
# version: 0.0.5
# author: Achilleas Pipinellis
# url: https://gitlab.com/gitlab-org/discourse-omniauth-gitlab

gem 'omniauth-gitlab', '1.0.2'
require 'auth/oauth2_authenticator'

class GitLabAuthenticator < ::Auth::OAuth2Authenticator

  GITLAB_APP_ID = ENV['GITLAB_APP_ID']
  GITLAB_SECRET = ENV['GITLAB_SECRET']

  def name
    'gitlab'
  end

  def after_authenticate(auth_token)
    result = super

    if result.user && result.email && (result.user.email != result.email)
      begin
        result.user.primary_email.update!(email: result.email)
      rescue
        used_by = User.find_by_email(result.email)&.username
        Rails.loger.warn("FAILED to update email for #{user.username} to #{result.email} cause it is in use by #{used_by}")
      end
    end

    result
  end

  def register_middleware(omniauth)
    omniauth.provider :gitlab,
     GITLAB_APP_ID,
     GITLAB_SECRET,
     {
       client_options:
       {
         site: ENV['GITLAB_URL']
       }
     }
  end
end


auth_provider title: 'with GitLab',
    message: 'Log in via GitLab (Make sure pop up blockers are not enabled).',
    frame_width: 920,
    frame_height: 800,
    authenticator: GitLabAuthenticator.new(
      'gitlab',
      auto_create_account: true,
      trusted: true
    )

# Use the fontawesome unicode for GitLab
# https://fontawesome.com/icons/gitlab?style=brands
register_css <<CSS

.btn-social.gitlab {
  background: #554488;
}

.btn-social.gitlab:before {
  content: "\f296";
}

CSS
